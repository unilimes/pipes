import {
    Object3D,
    RingGeometry,
    MeshBasicMaterial,
    Mesh,
    Vector3,
    Quaternion,
    Texture,
    Sprite,
    SpriteMaterial,
    DoubleSide
} from 'three-full';
import * as TWEEN from '@tweenjs/tween.js';

const COLORS = [
    0xff0000,
    0xffa500,
    0xffff00,
    0x00ff00
];
//3.28084
export default class Ellipse extends Object3D {
    constructor() {
        const delta = 100;
        super();
        this._index = 0;
        this.rotation.x = Math.PI / 2;
        this.holder = new Object3D();
        this.add(this.holder);
        this.currentSoil = (() => {
            const canvas = document.createElement('canvas');
            canvas.width = 128;
            canvas.height = 64;
            const context = canvas.getContext('2d');
            context.font = "12px Helvetica";
            context.textAlign = 'center';
            context.textBaseline = 'middle';
            context.fillStyle = 'rgba(0, 0, 0, 1.0)';
            context.fillText('', canvas.width / 2, canvas.height / 2);
            const texture = new Texture(canvas);
            texture.needsUpdate = true;
            const sprite = new Sprite(new SpriteMaterial({
                map: texture,
                depthTest: false
            }));
            sprite.scale.set(100, 50, 1.0);
            sprite.changeText = message => {
                // noinspection SillyAssignmentJS
                canvas.height = canvas.height;
                // noinspection SillyAssignmentJS
                canvas.width = canvas.width;
                context.font = "12px Helvetica";
                context.textAlign = 'center';
                context.textBaseline = 'middle';
                context.fillStyle = 'rgba(0, 0, 0, 1.0)';
                context.fillText(message, canvas.width / 2, canvas.height / 2);
                texture.needsUpdate = true;
            };
            return sprite;
        })();
        this.holder.add(this.currentSoil);
        ((delta) => {
            this._british = [];
            for (let i = 0; i < 4; i++) {
                const K = i * delta,
                    K_next = (i + 1) * delta;
                const ring = new Mesh(
                    new RingGeometry(K, K_next, 64, 16),
                    new MeshBasicMaterial({color: COLORS[i], side: DoubleSide, opacity: 0.7, transparent: true}));
                const text = label(K_next + ' ft.');
                const pos = (x, y, z) => {
                    const temp = text.clone();
                    temp.position.set(x, y, z);
                    this._british.push(temp);
                    this.holder.add(temp);
                };
                pos(K_next, 0, 0);
                pos(-K_next, 0, 0);
                pos(0, K_next, 0);
                pos(0, -K_next, 0);
                this.holder.add(ring);
                this._british.push(ring);
            }
        })(delta);
        ((delta) => {
            const ratio = 3.28084;
            this._metric = [];
            for (let i = 0; i < 4; i++) {
                const K = i * delta * ratio,
                    text_K_next = (i + 1) * delta,
                    K_next = text_K_next * ratio;
                const ring = new Mesh(
                    new RingGeometry(K, K_next, 64, 16),
                    new MeshBasicMaterial({color: COLORS[i], side: DoubleSide, opacity: 0.7, transparent: true}));
                ring.visible = false;
                const text = label(text_K_next + ' m');
                text.visible = false;
                const pos = (x, y, z) => {
                    const temp = text.clone();
                    temp.position.set(x, y, z);
                    this._metric.push(temp);
                    this.holder.add(temp);
                };
                pos(K_next, 0, 0);
                pos(-K_next, 0, 0);
                pos(0, K_next, 0);
                pos(0, -K_next, 0);
                this.holder.add(ring);
                this._metric.push(ring);
            }
        })(delta / 4);
        this.tween = new TWEEN.default.Tween({alpha: 0}).to({alpha: 1})
            .easing(TWEEN.default.Easing.Sinusoidal.Out)
            .onStart(params => {
                params.alpha = 0;
            })
            .onComplete(params => {
                params.alpha = 0;
            });
    }

    set isBritishSystem(bool) {
        bool = Boolean(bool);
        for (let i = 0, len = this._metric.length; i < len; i++) {
            this._metric[i].visible = !bool;
        }
        for (let i = 0, len = this._british.length; i < len; i++) {
            this._british[i].visible = bool;
        }
    }

    update(position, rotation) {
        this.holder.position.copy(position);
        this.holder.rotation.setFromQuaternion(
            new Quaternion().setFromUnitVectors(new Vector3(0, 0, 1), rotation));
    }
}

const label = message => {
    const canvas = document.createElement('canvas');
    canvas.width = 64;
    canvas.height = 16;
    const context = canvas.getContext('2d');
    context.font = "20px Helvetica";
    context.textAlign = 'center';
    context.textBaseline = 'middle';
    context.fillStyle = 'rgba(0, 0, 0, 1.0)';
    context.fillText(message, canvas.width / 2, canvas.height / 2);
    const texture = new Texture(canvas);
    texture.needsUpdate = true;
    const sprite = new Sprite(new SpriteMaterial({
        map: texture,
        depthTest: false
    }));
    sprite.scale.set(40, 10, 1.0);
    return sprite;
};