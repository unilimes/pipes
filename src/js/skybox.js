import {
    Mesh,
    SphereGeometry,
    ShaderMaterial,
    BackSide
} from 'three-full';

export default (params, radius, add) => {
    params.side = BackSide;
    params.vertexShader = vertexShader;
    params.fragmentShader = fragmentShader;
    const sky = new Mesh(
        new SphereGeometry(radius, 32, 32),
        new ShaderMaterial(params));
    add(sky);
}
const vertexShader = `varying vec3 vWorldPosition;
			void main() {
				vec4 worldPosition = modelMatrix * vec4( position, 1.0 );
				vWorldPosition = worldPosition.xyz;
				gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
			}`;
const fragmentShader = `uniform vec3 topColor;
			uniform vec3 bottomColor;
			uniform float offset;
			uniform float exponent;
			varying vec3 vWorldPosition;
			void main() {
				float h = normalize( vWorldPosition + offset ).y;
				gl_FragColor = vec4( mix( bottomColor, topColor, max( pow( max( h , 0.0), exponent ), 0.0 ) ), 1.0 );
			}`;