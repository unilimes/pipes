import Surface from './surface';
import SkyBox from './skybox';
import DimensionalGrid from './dimensionalGrid';

export default (viewer, box3, changeListener, formations) => {
    const surface = new Surface(box3, formations, obj => {
        viewer.scene.add(obj);
    });
    const X = box3.max.x - box3.min.x,
        Y = box3.max.y - box3.min.y,
        Z = box3.max.z - box3.min.z,
        MAX = Math.max(X, Y, Z);
    viewer.controls.target.copy(box3.getCenter());
    viewer.light.position.copy(box3.getCenter());
    viewer.light.position.y += Y;
    viewer.camera.position.copy(box3.getCenter());
    viewer.camera.position.y = 0;
    viewer.camera.position.z += (X * 1.5);
    viewer.controls.maxDistance = MAX * 48;
    SkyBox({
        uniforms: {
            topColor: {value: viewer.light.color.clone()},
            bottomColor: {value: viewer.light.groundColor.clone()},
            offset: {value: 33},
            exponent: {value: 0.6}
        }
    }, MAX * 64, obj => {
        viewer.scene.add(obj)
    });
    const grid = new DimensionalGrid(box3, viewer.renderer.capabilities.getMaxAnisotropy(), changeListener);
    viewer.scene.add(grid);
    return {
        surface,
        grid
    }
};