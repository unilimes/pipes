import { Raycaster, Vector2 } from 'three-full';

export default class DragAndDrop {
    constructor(camera, domElement, tubes, onClick) {
        domElement = ( domElement !== undefined ) ? domElement : document;
        onClick = (onClick instanceof Function) ? onClick : ()=>{};
        const ray = new Raycaster();
        const pointerVector = new Vector2();
        setTimeout(() => {
            domElement.addEventListener('mousedown', onPointerDown);
            domElement.addEventListener('touchstart', onPointerDown);
        }, 0);

        const onPointerDown = event => {
            if (( event.button !== undefined && event.button !== 0 )) return;
            const pointer = event.changedTouches ? event.changedTouches[0] : event;
            if (pointer.button === 0 || pointer.button === undefined) {
                const intersect = intersectObjects(pointer, tubes);
                if (intersect) {
                    event.preventDefault();
                    event.stopPropagation();
                    onClick(intersect.point, intersect.object);
                }
            }
        };
        const intersectObjects = (pointer, objects) => {
            const rect = domElement.getBoundingClientRect();
            const x = ( pointer.clientX - rect.left ) / rect.width;
            const y = ( pointer.clientY - rect.top ) / rect.height;
            pointerVector.set(( x * 2 ) - 1, -( y * 2 ) + 1);
            ray.setFromCamera(pointerVector, camera);
            const intersections = ray.intersectObjects(objects, true);
            return intersections[0] ? intersections[0] : false;
        };
    }
}