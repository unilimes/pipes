import {
    Vector3,
    Box3
} from 'three-full';
import * as TWEEN from '@tweenjs/tween.js';
import Tube from './tube';

export default class ActiveTube extends Tube {
    constructor(points) {
        super(points);
        this.material.color.setHex(0x0000ff);
        this.material.transparent = false;
        this.material.opacity = 1;
        this.material.color.setHex(0x0000ff);
        this.box3 = new Box3().setFromObject(this);
        this.direction = (() => {
            const arr = [];
            arr.push(new Vector3(0, 0, -1));
            const temp = this.points;
            for (let i = 1, len = temp.length; i < len; i++) {
                arr.push(temp[i].clone().sub(temp[i - 1]));
            }
            return arr;
        })();
        this._points = this.points;
        this.closestPoint = point => {
            const temp = {
                distance: Infinity,
                index: null
            };
            for (let i = 0, len = this._points.length; i < len; i++) {
                const myPoint = this._points[i].clone();
                const distance = myPoint.distanceTo(point);
                if (distance < temp.distance) {
                    temp.distance = distance;
                    temp.index = i;
                }
            }
            return temp.index;
        };
        this.geometry.setDrawRange(0, this.geometry.index.count);
        this.animateDraw = new TWEEN.default.Tween({alpha: 0}).to({alpha: 1})
            .easing(TWEEN.default.Easing.Sinusoidal.InOut)
            .onStart(params => {
                params.alpha = 0;
            })
            .onComplete(params => {
                params.alpha = 0;
            });
        this._radialRatio = this.geometry.index.count / this.geometry.parameters.tubularSegments;
        this._lengthTube = (() => {
            let total = 0;
            const points = [0];
            for (let i = 1, len = this._points.length; i < len; i++) {
                total += this._points[i - 1].clone().distanceTo(this._points[i].clone());
                points.push(total);
            }
            return {total, points};
        })();
    }

    // animate() {
    //     new TWEEN.default.Tween({alpha: 0}).to({alpha: 1}, 5000).onUpdate(params => {
    //         this.drawPart(params.alpha);
    //     }).start();
    // }

    get points() {
        return this.geometry.parameters.path.getPoints(this.geometry.parameters.path.points.length * 16);
    }

    drawPart(alpha) {
        this.animateDraw.stop();
        this.animateDraw._object = {alpha: 0};
        const index = Math.floor((this._points.length - 1) * alpha);
        const delta = this._lengthTube.points[index] / this._lengthTube.total;
        const currentDrawRange = this.geometry.drawRange.count,
            distanceDrawRange = Math.ceil(this.geometry.index.count * delta) + this._radialRatio - currentDrawRange;
        this.animateDraw.onUpdate(params => {
            this.geometry.setDrawRange(0,
                Math.ceil((currentDrawRange + (distanceDrawRange * params.alpha)) / this._radialRatio) * this._radialRatio);
        }).start();
    }

}