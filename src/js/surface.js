import {
    Object3D,
    Mesh,
    MeshLambertMaterial,
    BoxGeometry,
    BoxHelper,
    Texture,
    SpriteMaterial,
    Sprite,
    Box3,
    DoubleSide,
    LinearFilter
} from 'three-full';

export default class Surface extends Object3D {
    constructor(box3, soil, add) {
        super();
        add(this);
        this.position.copy(box3.getCenter());
        this.position.setY(0);
        this.checkActiveSoil = () => {};
        this.nameOfActiveSoil = () => { return ''};
        (() => {
            const X = (box3.max.x - box3.min.x) * 2,
                Z = X / 3 < ((box3.max.z - box3.min.z) * 2) ? ((box3.max.z - box3.min.z) * 2) : X / 3;
            const material = (color = 0x00000) => (new MeshLambertMaterial({
                color: color,
                transparent: true,
                opacity: 0.025,
                side: DoubleSide,
                depthTest: false
            }));
            if (!soil) {
                const materials = () => {
                    const basicSide = material(0x463014);
                    const grassSide = material(0x4DBD33);
                    return [
                        basicSide,
                        basicSide,
                        grassSide,
                        basicSide,
                        basicSide,
                        basicSide
                    ];
                };
                this.add(new Mesh(new BoxGeometry(X, (box3.max.y - box3.min.y), Z), materials()));
                this.position.setY((box3.max.y + box3.min.y) / 2);
                const helper = new BoxHelper(this, 0x463014);
                this.parent.add(helper);
                this.surfaceVisibility = bool => {
                    this.visible = helper.visible = bool;
                };
            } else {
                const checker = [];
                const alpha = (i, len) => ((Math.random() * i / len) || (i / len));
                const label = (message, opts) => {
                    const parameters = opts || {};
                    const fontface = parameters.fontface || 'Helvetica';
                    const fontsize = parameters.fontsize || 16;
                    const canvas = document.createElement('canvas');
                    const context = canvas.getContext('2d');
                    context.font = fontsize + "px " + fontface;
                    context.textAlign = 'center';
                    context.textBaseline = 'middle';
                    context.fillStyle = 'rgba(0, 0, 0, 1.0)';
                    context.fillText(message, canvas.width / 2, canvas.height / 2);
                    const texture = new Texture(canvas);
                    texture.minFilter = LinearFilter;
                    texture.needsUpdate = true;
                    return new Sprite(new SpriteMaterial({
                        map: texture,
                        depthTest: false
                    }));
                };
                const bottom = soil[soil.length - 1].bottom,
                    scaleX = bottom / 4,
                    scaleY = bottom / 8;
                let activeName = soil[0].name;
                for (let i = 0, len = soil.length; i < len; i++) {
                    const Y = soil[i].bottom - soil[i].top;
                    const mesh = new Mesh(new BoxGeometry(X, Y, Z), material());
                    if (!soil[i].color) {
                        mesh.material.color.setRGB(alpha(i, len), alpha(i, len), alpha(i, len));
                    } else {
                        mesh.material.color.setHex('0x' + soil[i].color);
                    }
                    mesh.position.setY(-soil[i].top - Y / 2);
                    mesh.name = soil[i].name;
                    const _box3 = new Box3().setFromObject(mesh);
                    const edges = new BoxHelper(mesh, mesh.material.color.getHex());
                    edges.position.copy(mesh.position.clone());
                    edges.material.transparent = true;
                    edges.material.opacity = 0.25;
                    this.add(edges);
                    const sprite = label(mesh.name);
                    sprite.position.setX(-X / 1.9);
                    sprite.scale.set(scaleX, scaleY, 1.0);
                    mesh.add(sprite);
                    this.add(mesh);
                    checker.push(y => {
                        if (y > _box3.min.y && y <= _box3.max.y) {
                            mesh.material.opacity = 0.25;
                            edges.material.opacity = 1;
                            activeName = mesh.name;
                        } else {
                            mesh.material.opacity = 0.025;
                            edges.material.opacity = 0.25;
                        }
                    })
                }
                const checkerLength = checker.length;
                this.checkActiveSoil = y => {
                    for (let i = 0; i < checkerLength; i++) {
                        checker[i](y);
                    }
                };
                this.nameOfActiveSoil = () => {
                    return activeName;
                };
                this.surfaceVisibility = bool => {
                    this.visible = bool;
                };
            }
        })();
        this.getSurfaceVisibility = () => (Boolean(this.visible));
    }
}