import {
    Object3D,
    Mesh,
    Texture,
    SpriteMaterial,
    Sprite,
    MeshBasicMaterial,
    BoxGeometry,
    Vector3,
    RepeatWrapping,
    BackSide
} from 'three-full';

export default class DimensionalGrid extends Object3D {
    constructor(box3, maxAnisotropy, changeListener) {
        super();
        this.box3 = box3;
        this.FACTOR = 500;
        this.SYSTEM = 'ft.';
        this.xAxis = new Object3D();
        this.yAxis = new Object3D();
        this.zAxis = new Object3D();
        this.init(maxAnisotropy, changeListener);
    }

    get MULTIPLIER() {
        return this.SYSTEM === 'ft.' ? 1 : 3.28084;
    }

    set isBritishSystem(bool) {
        this.SYSTEM = Boolean(bool) ? 'ft.' : 'm';
        this.FACTOR = Boolean(bool) ? 500 : 100;
        this.updateSystem();
    }

    init(maxAnisotropy, changeListener) {
        const box3 = this.box3;
        this.xAxis.clear = () => {
            const axis = this.xAxis;
            const array = axis.children;
            axis.big = [];
            axis.small = [];
            for (let i = array.length - 1; i >= 0; i--) {
                const obj = array[i];
                obj.material.map.dispose();
                obj.material.dispose();
                axis.remove(obj);
            }
        };
        this.xAxis.big = [];
        this.xAxis.small = [];
        this.yAxis.clear = () => {
            const axis = this.yAxis;
            const array = axis.children;
            axis.big = [];
            axis.small = [];
            for (let i = array.length - 1; i >= 0; i--) {
                const obj = array[i];
                obj.material.map.dispose();
                obj.material.dispose();
                axis.remove(obj);
            }
        };
        this.yAxis.big = [];
        this.yAxis.small = [];
        this.zAxis.clear = () => {
            const axis = this.zAxis;
            const array = axis.children;
            axis.big = [];
            axis.small = [];
            for (let i = array.length - 1; i >= 0; i--) {
                const obj = array[i];
                obj.material.map.dispose();
                obj.material.dispose();
                axis.remove(obj);
            }
        };
        this.zAxis.big = [];
        this.zAxis.small = [];
        const K = this.FACTOR * this.MULTIPLIER;
        const OFFSET = new Vector3(
            ((K - (box3.max.x % K)) - (box3.min.x < 0 ? ( K + (box3.min.x % K)) : ( K - (box3.min.x % K)))) / 2,
            -(box3.min.y < 0 ? ( K + (box3.min.y % K)) : ( K - (box3.min.y % K))) / 2,
            ((K - (box3.max.z % K)) - (box3.min.z < 0 ? ( K + (box3.min.z % K)) : ( K - (box3.min.z % K)))) / 2);
        const check = value => ( value < 0 ? 'floor' : 'ceil');
        const iX_plus = Math[check(box3.max.x)](box3.max.x / K),
            iX_minus = Math[check(box3.min.x)](box3.min.x / K),
            iY = Math[check(box3.min.y)]((box3.min.y) / K),
            iZ_plus = Math[check(box3.max.z)](box3.max.z / K),
            iZ_minus = Math[check(box3.min.z)](box3.min.z / K);
        const x = Math.abs(iX_plus) * K + Math.abs(iX_minus) * K,
            y = Math.abs(iY) * K,
            z = Math.abs(iZ_plus) * K + Math.abs(iZ_minus) * K;
        const scaleLabels = (array, multiply, divide) => {
            for (let i = 0, len = array.length; i < len; i++) {
                array[i].scale.set(multiply, divide, 1.0);
            }
        };
        const visibleLabels = (array, bool) => {
            bool = Boolean(bool);
            for (let i = 0, len = array.length; i < len; i++) {
                array[i].visible = bool;
            }
        };
        const distanceTo = (point, array) => {
            let value = Infinity;
            for (let i = 0, len = array.length; i < len; i++) {
                const temp = point.distanceTo(array[i].parent.localToWorld(array[i].position.clone()));
                value = temp < value ? temp : value;
            }
            return value;
        };
        this.textures = (() => {
            frame.anisotropy = maxAnisotropy;
            const xTexture = frame.clone(),
                yTexture = frame.clone(),
                zTexture = frame.clone();
            return {
                x: xTexture,
                y: yTexture,
                z: zTexture,
                get needsUpdate() {
                    this.x.needsUpdate = true;
                    this.y.needsUpdate = true;
                    this.z.needsUpdate = true;
                }
            };
        })();
        this.xAxis.position.set(x / 2 - Math.abs(iX_plus) * K, -y / 2, z / 2 + K / 2);
        this.yAxis.position.set(x / 2 + K / 2, y / 2, z / 2 + K / 2);
        this.zAxis.position.set(x / 2 + K / 2, -y / 2, z / 2 - Math.abs(iZ_plus) * K);
        const feetText = (iDimension, k, set, axis) => {
            const arr = axis.big;
            const n = iDimension / Math.abs(iDimension);
            if (n > 0) {
                for (let i = n; i <= iDimension; i += n) {
                    const temp = label(i * k + ' ft.');
                    temp.position[set](i * k);
                    axis.add(temp);
                    arr.push(temp);
                }
            } else {
                for (let i = n; i >= iDimension; i += n) {
                    const temp = label(i * k + ' ft.');
                    temp.position[set](i * k);
                    axis.add(temp);
                    arr.push(temp);
                }
            }
        };
        (() => {
            const xLabel = label('0');
            const yLabel = label('0');
            const zLabel = label('0');
            this.xAxis.add(xLabel);
            this.xAxis.big.push(xLabel);
            this.yAxis.add(yLabel);
            this.yAxis.big.push(yLabel);
            this.zAxis.add(zLabel);
            this.zAxis.big.push(zLabel);
        })();
        feetText(iX_plus, K, 'setX', this.xAxis);
        feetText(iX_minus, K, 'setX', this.xAxis);
        feetText(iY, K, 'setY', this.yAxis);
        feetText(iZ_plus, K, 'setZ', this.zAxis);
        feetText(iZ_minus, K, 'setZ', this.zAxis);
        const smallFeetText = (iDimension, k, set, axis) => {
            const arr = axis.small;
            const RATIO = 5;
            iDimension *= RATIO;
            k /= RATIO;
            const n = iDimension / Math.abs(iDimension);
            if (n > 0) {
                for (let i = n; i <= iDimension; i += n) {
                    if (i % 5) {
                        const temp = label(i * k + ' ft.');
                        temp.visible = false;
                        temp.position[set](i * k);
                        axis.add(temp);
                        arr.push(temp);
                    }
                }
            } else {
                for (let i = n; i >= iDimension; i += n) {
                    if (i % 5) {
                        const temp = label(i * k + ' ft.');
                        temp.visible = false;
                        temp.position[set](i * k);
                        axis.add(temp);
                        arr.push(temp);
                    }
                }
            }
        };
        smallFeetText(iX_plus, K, 'setX', this.xAxis);
        smallFeetText(iX_minus, K, 'setX', this.xAxis);
        smallFeetText(iY, K, 'setY', this.yAxis);
        smallFeetText(iZ_plus, K, 'setZ', this.zAxis);
        smallFeetText(iZ_minus, K, 'setZ', this.zAxis);
        (() => {
            this.setRepeat(iX_plus - iX_minus, iY, iZ_plus - iZ_minus);
            scaleLabels(this.xAxis.big, K * 2, K / 2);
            scaleLabels(this.yAxis.big, K * 2, K / 2);
            scaleLabels(this.zAxis.big, K * 2, K / 2);
            scaleLabels(this.xAxis.small, K / 2, K / 8);
            scaleLabels(this.yAxis.small, K / 2, K / 8);
            scaleLabels(this.zAxis.small, K / 2, K / 8);
            const material = texture => (new MeshBasicMaterial({
                map: texture,
                transparent: true,
                // depthTest: false,
                side: BackSide
                // side: THREE.DoubleSide
            }));
            this.mesh = new Mesh(
                new BoxGeometry(x, y, z),
                [
                    material(this.textures.z),
                    material(this.textures.z),
                    material(this.textures.y),
                    material(this.textures.y),
                    material(this.textures.x),
                    material(this.textures.x)
                ]);
            this.mesh.position.copy(OFFSET.clone());
            this.add(this.mesh);
            this.mesh.add(this.xAxis);
            this.mesh.add(this.yAxis);
            this.mesh.add(this.zAxis);
        })();
        this.position.copy(box3.getCenter());
        changeListener('change', e => {
            const K = this.FACTOR * this.MULTIPLIER;
            const camera = e.target.object.position.clone();
            if (camera.distanceTo(this.mesh.parent.localToWorld(this.mesh.position.clone())) < Math.max(x, y, z)) {
                if (canvas.isOne) {
                    canvas.fiveRectangles();
                    this.textures.needsUpdate;
                }
            } else if (!canvas.isOne) {
                canvas.oneRectangle();
                this.textures.needsUpdate;
            }
            const DISTANCE = K * 3;
            const enable = array => (distanceTo(camera, array) < DISTANCE);
            if (enable(this.xAxis.big)) {
                visibleLabels(this.xAxis.small, true);
                scaleLabels(this.xAxis.big, K, K / 4);
            } else {
                visibleLabels(this.xAxis.small, false);
                scaleLabels(this.xAxis.big, K * 2, K / 2);
            }
            if (enable(this.yAxis.big)) {
                visibleLabels(this.yAxis.small, true);
                scaleLabels(this.yAxis.big, K, K / 4);
            } else {
                visibleLabels(this.yAxis.small, false);
                scaleLabels(this.yAxis.big, K * 2, K / 2);
            }
            if (enable(this.zAxis.big)) {
                visibleLabels(this.zAxis.small, true);
                scaleLabels(this.zAxis.big, K, K / 4);
            } else {
                visibleLabels(this.zAxis.small, false);
                scaleLabels(this.zAxis.big, K * 2, K / 2);
            }
        });
    }

    setRepeat(x, y, z) {
        this.textures.x.repeat.set(x, y);
        this.textures.y.repeat.set(x, z);
        this.textures.z.repeat.set(z, y);
        this.textures.needsUpdate;
    }

    clear() {
        this.xAxis.clear();
        this.yAxis.clear();
        this.zAxis.clear();
    }

    updateSystem() {
        const box3 = this.box3;
        this.clear();
        const K = this.FACTOR * this.MULTIPLIER;
        const OFFSET = new Vector3(
            ((K - (box3.max.x % K)) - (box3.min.x < 0 ? ( K + (box3.min.x % K)) : ( K - (box3.min.x % K)))) / 2,
            -(box3.min.y < 0 ? ( K + (box3.min.y % K)) : ( K - (box3.min.y % K))) / 2,
            ((K - (box3.max.z % K)) - (box3.min.z < 0 ? ( K + (box3.min.z % K)) : ( K - (box3.min.z % K)))) / 2);
        const check = value => ( value < 0 ? 'floor' : 'ceil');
        const iX_plus = Math[check(box3.max.x)](box3.max.x / K),
            iX_minus = Math[check(box3.min.x)](box3.min.x / K),
            iY = Math[check(box3.min.y)]((box3.min.y) / K),
            iZ_plus = Math[check(box3.max.z)](box3.max.z / K),
            iZ_minus = Math[check(box3.min.z)](box3.min.z / K);
        const x = Math.abs(iX_plus) * K + Math.abs(iX_minus) * K,
            y = Math.abs(iY) * K,
            z = Math.abs(iZ_plus) * K + Math.abs(iZ_minus) * K;
        this.xAxis.position.set(x / 2 - Math.abs(iX_plus) * K, -y / 2, z / 2 + K / 2);
        this.yAxis.position.set(x / 2 + K / 2, y / 2, z / 2 + K / 2);
        this.zAxis.position.set(x / 2 + K / 2, -y / 2, z / 2 - Math.abs(iZ_plus) * K);
        const feetText = (iDimension, factor, k, set, axis, system) => {
            const kb = k * 2,
                ks = k / 2;
            const arr = axis.big;
            const n = iDimension / Math.abs(iDimension);
            if (n > 0) {
                for (let i = n; i <= iDimension; i += n) {
                    const temp = label(i * factor + ' ' + system);
                    temp.position[set](i * k);
                    temp.scale.set(kb, ks, 1.0);
                    axis.add(temp);
                    arr.push(temp);
                }
            } else {
                for (let i = n; i >= iDimension; i += n) {
                    const temp = label(i * factor + ' ' + system);
                    temp.position[set](i * k);
                    temp.scale.set(kb, ks, 1.0);
                    axis.add(temp);
                    arr.push(temp);
                }
            }
        };
        const smallFeetText = (iDimension, factor, k, set, axis, system) => {
            const kb = k / 2,
                ks = k / 8;
            const arr = axis.small;
            const RATIO = 5;
            iDimension *= RATIO;
            factor /= RATIO;
            k /= RATIO;
            const n = iDimension / Math.abs(iDimension);

            if (n > 0) {
                for (let i = n; i <= iDimension; i += n) {
                    if (i % 5) {
                        const temp = label(i * factor + ' ' + system);
                        temp.visible = false;
                        temp.position[set](i * k);
                        temp.scale.set(kb, ks, 1.0);
                        axis.add(temp);
                        arr.push(temp);
                    }
                }
            } else {
                for (let i = n; i >= iDimension; i += n) {
                    if (i % 5) {
                        const temp = label(i * factor + ' ' + system);
                        temp.visible = false;
                        temp.position[set](i * k);
                        temp.scale.set(kb, ks, 1.0);
                        axis.add(temp);
                        arr.push(temp);
                    }
                }
            }
        };
        feetText(iX_plus, this.FACTOR, K, 'setX', this.xAxis, this.SYSTEM);
        feetText(iX_minus, this.FACTOR, K, 'setX', this.xAxis, this.SYSTEM);
        feetText(iY, this.FACTOR, K, 'setY', this.yAxis, this.SYSTEM);
        feetText(iZ_plus, this.FACTOR, K, 'setZ', this.zAxis, this.SYSTEM);
        feetText(iZ_minus, this.FACTOR, K, 'setZ', this.zAxis, this.SYSTEM);
        smallFeetText(iX_plus, this.FACTOR, K, 'setX', this.xAxis, this.SYSTEM);
        smallFeetText(iX_minus, this.FACTOR, K, 'setX', this.xAxis, this.SYSTEM);
        smallFeetText(iY, this.FACTOR, K, 'setY', this.yAxis, this.SYSTEM);
        smallFeetText(iZ_plus, this.FACTOR, K, 'setZ', this.zAxis, this.SYSTEM);
        smallFeetText(iZ_minus, this.FACTOR, K, 'setZ', this.zAxis, this.SYSTEM);
        (() => {
            const xLabel = label('0');
            xLabel.scale.set(K*2, K/2, 1.0);
            const yLabel = label('0');
            yLabel.scale.set(K*2, K/2, 1.0);
            const zLabel = label('0');
            zLabel.scale.set(K*2, K/2, 1.0);
            this.xAxis.add(xLabel);
            this.xAxis.big.push(xLabel);
            this.yAxis.add(yLabel);
            this.yAxis.big.push(yLabel);
            this.zAxis.add(zLabel);
            this.zAxis.big.push(zLabel);
        })();
        this.mesh.geometry.dispose();
        this.mesh.geometry = new BoxGeometry(x, y, z);
        this.setRepeat(iX_plus - iX_minus, iY, iZ_plus - iZ_minus);
        this.mesh.position.copy(OFFSET.clone());
    }
}

const canvas = (() => {
    const canvas = document.createElement('canvas');
    canvas.width = 256;
    canvas.height = 256;
    const context = canvas.getContext('2d');
    canvas.isOne = false;
    canvas.fiveRectangles = () => {
        canvas.isOne = false;
        // noinspection SillyAssignmentJS
        canvas.width = canvas.width;
        // noinspection SillyAssignmentJS
        canvas.height = canvas.height;
        context.lineWidth = 4;
        const k = Math.floor(context.lineWidth / 2);
        context.rect(k, k, canvas.width - 2 * k, canvas.height - 2 * k);
        context.strokeStyle = "black";
        context.stroke();
        context.lineWidth = 1;
        const MULTIPLIER = 5;
        for (let i = 0; i < MULTIPLIER; i++) {
            for (let j = 0; j < MULTIPLIER; j++) {
                context.rect(i * canvas.width / MULTIPLIER, j * canvas.height / MULTIPLIER, canvas.width / MULTIPLIER, canvas.height / MULTIPLIER);
            }
        }
        context.stroke();
    };
    canvas.oneRectangle = () => {
        canvas.isOne = true;
        // noinspection SillyAssignmentJS
        canvas.width = canvas.width;
        // noinspection SillyAssignmentJS
        canvas.height = canvas.height;
        context.lineWidth = 4;
        const k = Math.floor(context.lineWidth / 2);
        context.rect(k, k, canvas.width - 2 * k, canvas.height - 2 * k);
        context.strokeStyle = "black";
        context.stroke();
    };
    canvas.oneRectangle();
    return canvas;
})();
const frame = (() => {
    const texture = new Texture(canvas);
    texture.wrapS = texture.wrapT = RepeatWrapping;
    texture.offset.set(0, 0);
    texture.needsUpdate = true;
    return texture;
})();
const label = message => {
    const canvas = document.createElement('canvas');
    canvas.width = 256;
    canvas.height = 64;
    const context = canvas.getContext('2d');
    context.font = "20px Helvetica";
    context.textAlign = 'center';
    context.textBaseline = 'middle';
    context.fillStyle = 'rgba(0, 0, 0, 1.0)';
    context.fillText(message, canvas.width / 2, canvas.height / 2);
    const texture = new Texture(canvas);
    texture.needsUpdate = true;
    const sprite = new Sprite(new SpriteMaterial({
        map: texture,
        depthTest: false
    }));
    sprite.scale.set(100, 25, 1.0);
    return sprite;
};
