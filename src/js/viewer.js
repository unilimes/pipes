import {
    Scene,
    WebGLRenderer,
    PerspectiveCamera,
    OrbitControls,
    HemisphereLight,
    Color,
    Fog,
    MOUSE
} from 'three-full';
import * as TWEEN from '@tweenjs/tween.js';
// import resize from './assets/resize';
// import ResizeSensor from 'css-element-queries/src/ResizeSensor';

export default class {
    constructor(root) {
        this._root_id = root;
        this.container = document.createElement('div');
        this.container.style.height = '100%';
        this.container.style.width = '100%';
        this.scene = new Scene();
        this.scene.name = 'Scene';
        this.scene.background = new Color().setHSL(0.6, 0, 1);
        this.scene.fog = new Fog(this.scene.background, 1, 10000000);
        this.renderer = new WebGLRenderer({antialias: true, alpha: true});
        this.renderer.setSize(this.container.offsetWidth, this.container.offsetHeight);
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.sortObjects = false;
        this.renderer.gammaInput = true;
        this.renderer.gammaOutput = true;
        this.renderer.shadowMap.enabled = true;
        this.renderer.localClippingEnabled = true;
        this.container.appendChild(this.renderer.domElement);
        this.camera = new PerspectiveCamera(50, this.container.offsetWidth / this.container.offsetHeight, 0.1, 10000000);
        this.camera.position.set(0, 150000, 200000);
        this.scene.add(this.camera);
        this.controls = new OrbitControls(this.camera, this.renderer.domElement);
        this.controls.enableKeys = false;
        this.controls.zoomSpeed = 5;
        this.controls.enableRotate = false;
        this.controls.enablePan = false;
        this.controls.enableZoom = false;
        this.controls.maxDistance = 400000;
        this.controls.mouseButtons = {
            PAN: MOUSE.LEFT
        }
        this.light = new HemisphereLight(0x87CEFA, 0xffffff, 1);
        this.light.groundColor.setHSL(0.095, 1, 0.75);
        this.scene.add(this.light);
        this.scene.fog.color.copy(this.light.groundColor.clone());

        this.cameraAnimation = new TWEEN.default.Tween({alpha: 0}).to({alpha: 1})
            .easing(TWEEN.default.Easing.Sinusoidal.InOut)
            .onStart(params => {
                params.alpha = 0;
                this.disableControls();
            })
            .onComplete(params => {
                params.alpha = 0;
                this.enableControls();
            });

        this.updateCamera = (target, position) => {
            this.cameraAnimation.stop();
            this.cameraAnimation._object = {alpha: 0};
            const currentCamPosition = this.camera.position.clone(),
                currentTargetPosition = this.controls.target.clone(),
                distantCamPosition = position ? position.clone() : target.clone().add(this.normalizedAngleVector.multiplyScalar(500)),
                distantTargetPosition = target.clone();
            const onUpdate = params => {
                this.camera.position.copy(currentCamPosition.clone().lerp(distantCamPosition.clone(), params.alpha));
                this.controls.target.copy(currentTargetPosition.clone().lerp(distantTargetPosition.clone(), params.alpha));
                this.controls.update();
            };
            this.cameraAnimation.onUpdate(onUpdate).start();
        };

        this.resetCamera = (target, position) => {
            const currentCamPosition = this.camera.position.clone(),
                currentTargetPosition = this.controls.target.clone(),
                distantCamPosition = position.clone(),
                distantTargetPosition = target.clone();
            const onUpdate = params => {
                this.camera.position.copy(currentCamPosition.clone().lerp(distantCamPosition.clone(), params.alpha));
                this.controls.target.copy(currentTargetPosition.clone().lerp(distantTargetPosition.clone(), params.alpha));
                this.controls.update();
            };
            this.cameraAnimation.onUpdate(onUpdate).start();
        };

        this._updateCamera = (target, point) => {
            const currentCamPosition = this.camera.position.clone(),
                currentTargetPosition = this.controls.target.clone(),
                distantCamPosition = point.clone(),
                distantTargetPosition = target.clone();
            const onUpdate = (params) => {
                this.camera.position.copy(currentCamPosition.clone().lerp(distantCamPosition.clone(), params.alpha));
                this.controls.target.copy(currentTargetPosition.clone().lerp(distantTargetPosition.clone(), params.alpha));
                this.controls.update();
            };
            this.cameraAnimation.onUpdate(onUpdate).start();
        };
        this.dimensionsUpdate = () => {
            this.camera.aspect = this.container.offsetWidth / this.container.offsetHeight;
            this.camera.updateProjectionMatrix();
            this.renderer.setSize(this.container.offsetWidth, this.container.offsetHeight);
        };
        this.controls.addEventListener('change', (event) => {
            this.renderer.render(this.scene, this.camera);
        });
        // ResizeSensor(this.parent, () => {
        //     this.dimensionsUpdate()
        // });
    }

    disableControls() {
        this.controls.enableRotate = false;
        this.controls.enablePan = false;
        this.controls.enableZoom = false;
    };

    enableControls() {
        // this.controls.enableRotate = true;
        // this.controls.enablePan = true;
        // this.controls.enableZoom = true;
    };

    cancelAnimation() {
        if (this.animationID) {
            cancelAnimationFrame(this.animationID);
            this.animationID = null;
        }
    }

    startAnimation() {
        if (!this.animationID) {
            this.animate();
        }
    }

    attach() {
        this.parent.appendChild(this.container);
        this.startAnimation();
    }

    detach() {
        this.parent.removeChild(this.container);
        this.cancelAnimation();
    }

    get parent() {
        return document.getElementById(this._root_id);
    }

    get normalizedAngleVector() {
        return this.camera.position.clone().sub(this.controls.target.clone()).normalize();
    }

    animate() {
        this.controls.update();
        TWEEN.default.update();
        this.renderer.render(this.scene, this.camera);
        this.animationID = requestAnimationFrame(this.animate.bind(this));
    }
}