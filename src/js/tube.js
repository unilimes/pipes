import {
    Mesh,
    Vector3,
    TubeBufferGeometry,
    CatmullRomCurve3,
    MeshPhysicalMaterial,
    DoubleSide
} from 'three-full';

export default class Tube extends Mesh {
    constructor(points) {
        const _points = [];
        for (let i = 0, len = points.length; i < len; i++) {
            _points.push(new Vector3(points[i].x, points[i].y, points[i].z));
        }
        super(new TubeBufferGeometry(
            new CatmullRomCurve3(_points, false), _points.length * 16, 20, 16, false),
            new MeshPhysicalMaterial({
                color: 0xa5a5a5,
                side: DoubleSide,
                transparent: true,
                opacity: 0.75
            }));
        this.rotation.x = Math.PI / 2;
    }
}