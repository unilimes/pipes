import {Object3D, Box3} from 'three-full';
import * as TWEEN from '@tweenjs/tween.js';
import Viewer from './viewer';
import ActiveTube from './activeTube';
import Tube from './tube';
import Ellipse from './ellipse';
import Init from './init';
import RayCast from './raycast';

export class TubeApp {
    constructor(root, tube_coordinates, config = {}) {
        this._disabledByCombo = false;
        this.viewer = new Viewer(root);
        const holder = new Object3D();
        this.viewer.scene.add(holder);
        this.tube = new ActiveTube(tube_coordinates);
        holder.add(this.tube);
        if (config.tubes) {
            for (let i = 0; i < config.tubes.length; i++) {
                holder.add(new Tube(config.tubes[i]));
            }
        }
        const box3 = new Box3().setFromObject(holder);
        this.ellipse = new Ellipse();
        this.viewer.scene.add(this.ellipse);
        const {surface, grid} = Init(this.viewer, box3,
            this.viewer.controls.addEventListener.bind(this.viewer.controls), config.soil);
        const onClick = (point, tube) => {
            const index = tube.closestPoint(this.tube.worldToLocal(point));
            const _point = this.tube._points[index].clone();
            _point.set(_point.x, -_point.z, _point.y);
            this.viewer.updateCamera(_point);
        };
        new RayCast(this.viewer.camera, this.viewer.renderer.domElement, [this.tube], onClick);
        this.soilVisibility = bool => {
            surface.surfaceVisibility(Boolean(bool));
        };
        this.getSoilVisibility = () => {
            return surface.getSurfaceVisibility();
        };
        this.isBritishSystem = bool => {
            grid.isBritishSystem = bool;
            this.ellipse.isBritishSystem = bool;
        };
        this.showEllipse = bool => {
            this.ellipse.visible = Boolean(bool);
        };
        this.updateDraw = alpha => {
            if (this._disabledByCombo) {
                return;
            }
            this.tube.drawPart(alpha);
        };
        this.updateEllipse = alpha => {
            if (this._disabledByCombo) {
                return;
            }
            this.ellipse.tween.stop();
            this.ellipse.tween._object = {alpha: 0};
            const index = Math.floor((this.tube._points.length - 1) * alpha),
                currentIndex = this.ellipse._index,
                diffIndex = index - currentIndex;
            this.ellipse.tween.onUpdate(params => {
                this.ellipse._index = currentIndex + Math.round(diffIndex * params.alpha);
                this.ellipse.update(this.tube._points[this.ellipse._index].clone(), this.tube.direction[this.ellipse._index]
                    .clone().normalize());
                surface.checkActiveSoil(-this.ellipse.holder.position.clone().z);
                this.ellipse.currentSoil.changeText(surface.nameOfActiveSoil());
            }).start();
        };
        this.updateCamera = alpha => {
            if (this._disabledByCombo) {
                return;
            }
            const index = Math.floor((this.tube._points.length - 1) * alpha);
            const point = this.tube._points[index].clone();
            point.set(point.x, -point.z, point.y);
            this.viewer.updateCamera(point.clone());
        };
        this.resetCamera = () => {
            const temp = this.tube.box3.getCenter();
            temp.y = 0;
            temp.z += ((this.tube.box3.max.x - this.tube.box3.min.x) * 1.5);
            this.viewer.updateCamera(this.tube.box3.getCenter(), temp);
        };
        this.zoomIn = () => {
            this.viewer.camera.fov *= 0.9;
            this.viewer.camera.updateProjectionMatrix();
        };
        this.zoomOut = () => {
            this.viewer.camera.fov *= 1.1;
            this.viewer.camera.updateProjectionMatrix();
        };
        this.allowPan = () => {
            this.viewer.controls.enablePan = !this.viewer.controls.enablePan
        };
        this.attach = () => {
            this.viewer.attach();
            this.viewer.dimensionsUpdate();
        };
        this.detach = () => {
            this.viewer.detach();
        };


        // this._animateCombo = new TWEEN.default.Tween({alpha: 0}).to({alpha: 1})
        //     .easing(TWEEN.default.Easing.Sinusoidal.Out)
        //     .onStart(params => {
        //         params.alpha = 0;
        //         this._disabledByCombo = true;
        //         this.viewer.disableControls();
        //     })
        //     .onComplete(params => {
        //         params.alpha = 0;
        //         this._disabledByCombo = false;
        //         this.viewer.enableControls();
        //     });
        this.updateCombo = alpha => {

            const index = Math.floor((this.tube._points.length - 1) * alpha),
                point = this.tube._points[index].clone();
            point.set(point.x, -point.z, point.y);

            const delta = this.tube._lengthTube.points[index] / this.tube._lengthTube.total;

            this.ellipse._index = index;
            this.ellipse.update(this.tube._points[index].clone(), this.tube.direction[index]
                .clone().normalize());
            surface.checkActiveSoil(-this.ellipse.holder.position.clone().z);
            this.ellipse.currentSoil.changeText(surface.nameOfActiveSoil());
            this.viewer.camera.position.copy(point.clone().add(this.viewer.normalizedAngleVector.multiplyScalar(1000)));
            this.viewer.controls.target.copy(point.clone());
            this.viewer.controls.update();
            this.tube.geometry.setDrawRange(0, Math.ceil
            ((this.tube.geometry.index.count * delta + this.tube._radialRatio)
                / this.tube._radialRatio) * this.tube._radialRatio);
        };


        if (config.initAnimation) {
            this.viewer.disableControls();
            const length = this.tube._points.length - 1;
            const RATIO = 1000;
            const distance = this.viewer.normalizedAngleVector.multiplyScalar(RATIO);
            const STOP_ANIMATION = {
                bool: false
            };
            const cancelAnimation = () => {
                STOP_ANIMATION.bool = true;
                this.viewer.renderer.domElement.removeEventListener('click', cancelAnimation);
            };
            this.viewer.renderer.domElement.addEventListener('click', cancelAnimation);
            const recursive = index => {
                if (index >= length || STOP_ANIMATION.bool) {
                    this.tube.drawPart(1);
                    this.updateEllipse(0);
                    this.resetCamera();
                    this.viewer.enableControls();
                    return;
                }


                const currentIndex = this.ellipse._index,
                    diffIndex = index - currentIndex,
                    point = this.tube._points[index].clone();
                point.set(point.x, -point.z, point.y);

                const currentCamPosition = this.viewer.camera.position.clone(),
                    currentTargetPosition = this.viewer.controls.target.clone(),
                    distantCamPosition = point.clone().add(distance.clone()),
                    distantTargetPosition = point.clone();

                const delta = this.tube._lengthTube.points[index] / this.tube._lengthTube.total,
                    currentDrawRange = this.tube.geometry.drawRange.count,
                    distanceDrawRange = Math.ceil(this.tube.geometry.index.count * delta) + this.tube._radialRatio - currentDrawRange;

                this.ellipse.update(this.tube._points[index].clone(), this.tube.direction[index]
                    .clone().normalize());
                this.viewer.camera.position.copy(distantCamPosition.clone());
                this.viewer.controls.target.copy(distantTargetPosition.clone());
                this.viewer.controls.update();
                this.tube.geometry.setDrawRange(0,
                    Math.ceil((this.tube.geometry.index.count * delta + this.tube._radialRatio) / this.tube._radialRatio) * this.tube._radialRatio);

                new TWEEN.default.Tween({alpha: 0}).to({alpha: 1}, 5)
                    .onUpdate(params => {
                        this.ellipse._index = currentIndex + Math.round(diffIndex * params.alpha);
                        this.ellipse.update(this.tube._points[this.ellipse._index].clone(), this.tube.direction[this.ellipse._index]
                            .clone().normalize());
                        this.viewer.camera.position.copy(currentCamPosition.clone().lerp(distantCamPosition.clone(), params.alpha));
                        this.viewer.controls.target.copy(currentTargetPosition.clone().lerp(distantTargetPosition.clone(), params.alpha));
                        this.viewer.controls.update();
                        this.tube.geometry.setDrawRange(0,
                            Math.ceil((currentDrawRange + (distanceDrawRange * params.alpha))
                                / this.tube._radialRatio) * this.tube._radialRatio);
                    })
                    .onComplete(() => {
                        surface.checkActiveSoil(-this.ellipse.holder.position.clone().z);
                        this.ellipse.currentSoil.changeText(surface.nameOfActiveSoil());
                        recursive(index + 5);
                    }).start();
            };
            recursive(0);
        }
    }
}