import {TubeApp} from './js/app';
import json from '../jsons/coord_2.json';
import soil from '../jsons/formations.json';
// import json from '../jsons/coord.json';

import tube1 from '../jsons/48F_6H.json';
import tube2 from '../jsons/48G_7H.json';
import tube3 from '../jsons/48H_8H.json';
import tube4 from '../jsons/48I_9H.json';
import tube5 from '../jsons/48J_10H.json';


window.TubeApp = class extends TubeApp {
    constructor(root) {
        super(root, tube5, {soil, tubes: [tube1, tube2, tube3, tube4], initAnimation: true}); //
        this.attach();
        (() => {
            const input = document.createElement('input');
            input.type = 'range';
            input.min = '0';
            input.max = '1';
            input.step = 0.0001;
            input.value = 0;
            input.style.position = 'absolute';
            input.style.top = '10%';
            input.style.left = '10%';
            input.style.transform = 'translate(-50%, -50%)';
            document.body.appendChild(input);
            input.addEventListener('input', () => {
                // this.updateCamera(parseFloat(input.value));
                // this.updateEllipse(parseFloat(input.value));
                // this.updateDraw(parseFloat(input.value));
                this.updateCombo(parseFloat(input.value));
            })
        })();
        (() => {
            const input = document.createElement('input');
            input.type = 'range';
            input.min = '0';
            input.max = '1';
            input.step = 0.0001;
            input.value = 0;
            input.style.position = 'absolute';
            input.style.top = '20%';
            input.style.left = '10%';
            input.style.transform = 'translate(-50%, -50%)';
            document.body.appendChild(input);
            input.addEventListener('input', () => {
                // this.updateCamera(parseFloat(input.value));
                this.updateEllipse(parseFloat(input.value));
            })
        })();
        (() => {
            const div = document.createElement('div');
            div.innerText = "Reset";
            div.style.position = 'absolute';
            div.style.top = '30%';
            div.style.left = '10%';
            div.style.transform = 'translate(-50%, -50%)';
            document.body.appendChild(div);
            div.addEventListener('click', () => {
                this.resetCamera();
            })
        })();
        (() => {
            const div = document.createElement('div');
            div.innerText = "+";
            div.style.position = 'absolute';
            div.style.top = '40%';
            div.style.left = '10%';
            div.style.transform = 'translate(-50%, -50%)';
            document.body.appendChild(div);
            div.addEventListener('click', () => {
                this.zoomIn();
            })
        })();
        (() => {
            const div = document.createElement('div');
            div.innerText = "-";
            div.style.position = 'absolute';
            div.style.top = '50%';
            div.style.left = '10%';
            div.style.transform = 'translate(-50%, -50%)';
            document.body.appendChild(div);
            div.addEventListener('click', () => {
                this.zoomOut();
            })
        })();
        (() => {
            const div = document.createElement('div');
            div.innerText = "pan";
            div.style.position = 'absolute';
            div.style.top = '60%';
            div.style.left = '10%';
            div.style.transform = 'translate(-50%, -50%)';
            document.body.appendChild(div);
            div.addEventListener('click', () => {
                this.allowPan();
            })
        })();
    }
};