# Tube Application #

Tubes, which was built by coordinates and immersed to the ground.

## INITIALIZATION ##

Call the constructor by `new TubeApp( id, MainTube, Config )`.
###### id ######
First parameter is a **String**, which is ***ID*** of the *HTML element*, where the app should be inserted.
###### Main tube ######
Second parameter is ***\*.json*** file. It has to be an array of objects with 3 fields,  named:
`x, y, z`, which is representing the coordinates of the tube will created by.
###### Config ######
Third parameter is not required. It's Config-Object, which may has up to 2 fields:  
1. **`soil`** -  ***\*.json*** file, which has to be an array of objects with 3 required fields `name, top, bottom` and 1 not required `color`.  
`name` is a **String**, which represents a name of the soil.  
`top` and `bottom` are **Numbers**(may be float), means from what and to what depth is that soil.  
`color` is a **String**, represents hexadecimal expression of this soil. Example: 'ff0000' is a red color.  
2. **`tubes`** - Array of the ***\*.json*** files. Requirements the same as for main tube.  
3. **`initAnimation`** - **Boolean**, if `true` - starts initial animation that follows camera and builds tube incrementally.  

## Methods ##
- `soilVisibility( visibility )` is a method, which takes ***Boolean*** as argument and makes the Soil visible or not.
- `getSoilVisibility()` returns current visibility of the soil.
- `isBritishSystem( isBritish )` takes ***Boolean*** as argument to transform system to British or Metric (default is British).
- `showEllipse()` takes ***Boolean*** as argument to show or hide the Ellipse (BullsEye).
- `updateDraw( alpha )` takes ***Float*** from 0 to 1.0, which shows alpha-part of the main tube.
- `updateEllipse( alpha )` takes ***Float*** from 0 to 1.0, which takes the ellipse to that alpha-part of the main tube.
- `updateCamera( alpha )` takes ***Float*** from 0 to 1.0, which takes the camera to that alpha-part of the main tube.
- `updateCombo(alpha)` takes ***Float*** from 0 to 1.0, which updates ellipse, camera and tube draw.
- `resetCamera()` resets the camera to the default position.
- `attach()` inserts the App to the root HTML Element with the id from the constructor.
- `detach()` removes the App from the root HTML Element.