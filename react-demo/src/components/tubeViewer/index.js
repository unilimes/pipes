import './index.scss';
import './controls/index.scss';
import React, {Component} from 'react';
import soil from '../../../../jsons/formations.json';
import staticTubes1 from '../../../../jsons/48F_6H.json';
import staticTubes2 from '../../../../jsons/48G_7H.json';
import staticTubes3 from '../../../../jsons/48H_8H.json';
import staticTubes4 from '../../../../jsons/48I_9H.json';
import mainPoints from '../../../../jsons/48J_10H.json';
import ReactTubeViewer1 from '../../../build/tube-viewer';
import ReactTubeViewer from '../../src-react-library/index';
import Time from "../time/index";
import Slider from 'react-rangeslider'

export class TubeViewer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isBritish: true,
            threeSliders: true,
            ellipseAlpha: 0,
            cameraAlpha: 0,
            comboAlpha: 0,
            drawAlpha: 1
        };
    }


    comboSliderEnable() {
        this.setState(prevState => ({threeSliders: !prevState.threeSliders}));
    }

    render() {
        const config = {
            soil: soil,
            tubes: [staticTubes1, staticTubes2, staticTubes3, staticTubes4],
            initAnimation: true
        };

        return (
            <div className='viewer'>
                <div className="left">
                    <ReactTubeViewer ref={viewer => {
                        if (!viewer) {
                            return;
                        }
                        this.controls = viewer._api;
                    }} mainTube={mainPoints} config={config}/>
                </div>
                <div className="right">
                    <div className="top"><Time ref="time"/></div>
                    <div className="bot">
                        <div className="controls">
                            <div className="btn" onClick={() => {
                                this.controls.soilVisibility(!this.controls.getSoilVisibility())
                            }}>
                                <div className="toggleText">Soil</div>
                            </div>
                            <div className="btn" onClick={() => {
                                this.controls.isBritishSystem(!this.state.isBritish);
                                this.setState({isBritish: !this.state.isBritish});
                            }}>
                                <div className="toggleText">{!this.state.isBritish ? 'British' : 'Metric'}</div>
                            </div>
                            <div className="btn" onClick={() => {
                                this.controls.resetCamera()
                            }}>
                                <div className="toggleText">Reset Camera</div>
                            </div>
                            <div className="btn" onClick={this.comboSliderEnable.bind(this)}>
                                <div
                                    className="toggleText">{this.state.threeSliders ? 'Three Sliders' : 'One Slider'}</div>
                            </div>
                            <div style={{position: 'absolute', width: '100%'}}>
                                {
                                    !this.state.threeSliders ?
                                        <div>
                                            <Slider
                                                max={1} step={0.001} value={this.state.ellipseAlpha} tooltip={false}
                                                onChange={value => {
                                                    this.controls.updateEllipse(value);
                                                    this.setState({ellipseAlpha: value});
                                                }}/>
                                            <Slider
                                                max={1} step={0.001} value={this.state.cameraAlpha} tooltip={false}
                                                onChange={value => {
                                                    this.controls.updateCamera(value);
                                                    this.setState({cameraAlpha: value});
                                                }}/>
                                            <Slider
                                                max={1} step={0.0001} value={this.state.drawAlpha} tooltip={false}
                                                onChange={value => {
                                                    this.controls.updateDraw(value);
                                                    this.setState({drawAlpha: value});
                                                }}/>
                                        </div> :
                                        <div>
                                            <Slider
                                                max={1} step={0.001} value={this.state.comboAlpha} tooltip={false}
                                                onChange={value => {
                                                    this.controls.updateCombo(value);
                                                    this.setState({
                                                        comboAlpha: value
                                                    });
                                                }}/>
                                        </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}