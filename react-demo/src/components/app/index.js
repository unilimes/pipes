import './index.scss';
import React, {Component} from 'react';
import {TubeViewer} from "../tubeViewer/index";

class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="application">
                <TubeViewer/>
            </div>
        );
    }
}

export default App;