import './index.scss';
import React, {Component} from 'react';

export default class Time extends Component {
    constructor(props) {
        super(props);
        const time = new Date();
        this.state = {
            hours: time.getHours(),
            minutes: time.getMinutes(),
            seconds: time.getSeconds()
        };

    }

    refresh() {
        const {seconds} = this.state,
            time = new Date();
        if (seconds !== time.getSeconds()) {
            this.setState({
                hours: time.getHours(),
                minutes: time.getMinutes(),
                seconds: time.getSeconds()
            });
        }
        this.animationID = requestAnimationFrame(this.refresh.bind(this));
    };

    componentDidMount(){
        this.refresh();
    }

    componentWillUnmount() {
        cancelAnimationFrame(this.animationID);
    }

    render() {
        const {hours, minutes, seconds} = this.state;
        const fixTime = time => (time > 9 ? time : '0' + time);
        return (
            <div className="time">
                {fixTime(hours)}<span className="dots">:</span>{fixTime(minutes)}<span
                className="dots">:</span>{fixTime(seconds)}
            </div>
        );
    }
}