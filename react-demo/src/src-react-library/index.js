import React, {Component} from 'react';
import {TubeApp} from '../umd-library/tubeApp';
import ResizeObserver from 'react-resize-observer'

export default class TubeViewer extends Component {
    constructor(props) {
        super(props);
        this._defaultId = 'tube-' + Math.ceil(Math.random() * 8999999 + 1000000) + '-viewer-' + Math.ceil(Math.random() * 8999999 + 1000000);
        this._api = new TubeApp(this._defaultId, props.mainTube, props.config);
        this.controls = {
            soilVisibility: this._api.soilVisibility,
            getSoilVisibility: this._api.getSoilVisibility,
            isBritishSystem: this._api.isBritishSystem,
            showEllipse: this._api.showEllipse,
            updateDraw: this._api.updateDraw,
            updateEllipse: this._api.updateEllipse,
            updateCamera: this._api.updateCamera,
            resize: this._api.viewer.dimensionsUpdate,
            resetCamera: this._api.resetCamera,
            zoomIn: this._api.zoomIn,
            zoomOut: this._api.zoomOut,
            allowPan: this._api.allowPan,
        };
        this._defaultStyle = {
            width: '100%',
            height: '100%'
        }
    }

    componentDidMount() {
        this._api.attach();
    }

    componentWillUnmount() {
        this._api.detach();
    }

    render() {
        return (
            <div id={this._defaultId} style={{...this._defaultStyle, ...this.props.style}}>
                <ResizeObserver
                    onResize={(rect) => {
                        this.controls.resize();
                    }}
                />
            </div>
        )
    }
}