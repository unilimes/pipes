const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');

const extractSass = new ExtractTextPlugin({
    filename: "dist/style.min.css"
});

const isProduction = (process.env.NODE_ENV === 'production');

module.exports = {
    entry: isProduction ?
        {
            'tube-viewer': path.join(__dirname, '/src/src-react-library/index.js')
        } :
        {
            polyfill: 'babel-polyfill',
            bundle: path.join(__dirname, '/src/index.js')
        },
    output: isProduction ?
        {
            path: path.join(__dirname, 'build'),
            library: 'tube-viewer',
            libraryTarget: 'commonjs2',
            filename: '[name].js'
        } :
        {
            publicPath: '',
            path: path.join(__dirname, 'www'),
            filename: 'dist/[name].js'
        },
    module: {
        rules: isProduction ? [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /(node_modules|bower_components)/,
                    loader: 'babel-loader',
                    query: {
                        compact: true,
                        babelrc: false,
                        presets: ['env'],
                        plugins: [
                            "transform-object-rest-spread",
                            "transform-react-jsx"
                        ]
                    }
                },
            ] :
            [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /(node_modules|bower_components|build)/,
                    loader: 'babel-loader',
                    query: {
                        compact: true,
                        babelrc: false,
                        cacheDirectory: true,
                        presets: ['env', 'react'],
                        plugins: [
                            "transform-object-rest-spread",
                            "transform-react-jsx"
                        ]
                    }
                },
                {
                    test: /\.(png|jpe?g|gif|svg)$/i,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                outputPath: 'assets/images/',
                                name: '[hash:8].[ext]'
                            }
                        }
                    ]
                },
                {
                    test: /\.scss$/,
                    use: extractSass.extract({
                        fallback: "style-loader",
                        use: [{
                            loader: "css-loader",
                            options: {
                                minimize: true
                            }
                        }, {
                            loader: "sass-loader"
                        }]
                    })
                }
            ]
    },
    externals: isProduction ?
        {
            react: {
                commonjs2: 'react',
                commonjs: 'react'
            }
        } : {},
    plugins: isProduction ? [] :
        [
            extractSass,
            new HtmlWebpackPlugin({
                template: 'src/index.html',
                filename: 'index.html',
                inject: 'body',
                defer: ['polyfill', 'bundle'],
                hash: true
            })
        ]
};