# Tube Application #

`npm run build:react-library` // production
`npm run demo` // dev
`npm run serve` // dev three.js


React-Component, which is in **/react-demo/build** folder.
Represents tubes, which was built by coordinates and immersed to the ground.

## INITIALIZATION ##

Import tube-viewer component and eject it in your React-App, as usual react-component.  Example:
```javascript
import TubeViewer from './build/tube-viewer'
...
<TubeViewer mainTube={YOUR_JSON} config={OPTIONS} ref={viewer => {
       if (!viewer) {
           return;
       }
       this.controls = viewer.controls;}/>
```

### Props ###

###### mainTube ######
This prop is required and it's ***\*.json*** file. It has to be an array of objects with 3 fields,  named:
`x, y, z`, which is representing the coordinates of the tube will created by.
###### config ######
This prop is not required. It's Config-Object, which may has up to 3 fields:  
1. **`soil`** -  ***\*.json*** file, which has to be an array of objects with 3 required fields `name, top, bottom` and 1 not required `color`.  
`name` is a **String**, which represents a name of the soil.  
`top` and `bottom` are **Numbers**(may be float), means from what and to what depth is that soil.  
`color` is a **String**, represents hexadecimal expression of this soil. Example: 'ff0000' is a red color.  
2. **`tubes`** - Array of the ***\*.json*** files. Requirements the same as for main tube.  
3. **`initAnimation`** - **Boolean**, if `true` - starts initial animation that follows camera and builds tube incrementally.

## Methods ##
###### All of them collected in `controls` object ######
- `soilVisibility(visibility)` is a method, which takes ***Boolean*** as argument and makes the Soil visible or not.
- `getSoilVisibility()` returns current visibility of the soil.
- `isBritishSystem(isBritish)` takes ***Boolean*** as argument to transform system to British or Metric (default is British).
- `showEllipse(visibility)` takes ***Boolean*** as argument to show or hide the Ellipse (BullsEye).
- `updateDraw(alpha)` takes ***Float*** from 0 to 1.0, which shows alpha-part of the main tube.
- `updateEllipse(alpha)` takes ***Float*** from 0 to 1.0, which takes the ellipse to that alpha-part of the main tube.
- `updateCamera(alpha)` takes ***Float*** from 0 to 1.0, which takes the camera to that alpha-part of the main tube.
- `updateCombo(alpha)` takes ***Float*** from 0 to 1.0, which updates ellipse, camera and tube draw.
- `resetCamera()` resets the camera to the default position.